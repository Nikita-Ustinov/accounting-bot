import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
from decouple import config

# Connect to Google Sheets
scope = ['https://www.googleapis.com/auth/spreadsheets',
         "https://www.googleapis.com/auth/drive"]

gs_credentials = config('gs_credentials')

credentials = ServiceAccountCredentials.from_json_keyfile_name(gs_credentials, scope)
client = gspread.authorize(credentials)


sheet = client.create("NewDatabase")
# sheet.share('ustio', perm_type='user', role='writer')


# Open the spreadsheet
sheet = client.open("NewDatabase").sheet1
# read csv with pandas
df = pd.read_csv('football_news')
# export df to a sheet
sheet.update([df.columns.values.tolist()] + df.values.tolist())


print('Done')