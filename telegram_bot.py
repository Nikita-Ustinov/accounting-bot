# Standard python library
import logging
from typing import Callable, List

# Third-party libraries
from decouple import config
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


class TelegramBot:

    def __init__(self, telegram_token: str="2042679017:AAFlPtBfKS0oTkjIAw-axFk9ZeNhZV-Nzrg"):
        self.token = telegram_token

    def run(self) -> None:
        '''! Blocking !'''
        updater = Updater(self.token, use_context=True)
        self.add_handlers(updater)
        logger.info('AccountingBot is running')
        updater.idle()

    @staticmethod
    def _start(update, context):
        """Send a message when the command /start is issued."""
        user_name = TelegramBot.get_user_name(update)
        logger.info(f"_start - user {user_name}")
        update.message.reply_text('Hi!')
        logger.info("_start")
    
    @staticmethod
    def _help(update, context):
        update.message.reply_text(f'You can type the next commands: {AccountingBot._get_all_commands()}')
        logger.info("_help")
    
    @staticmethod
    def _echo(update, context):
        """Echo the user message."""
        update.message.reply_text(update.message.text)
        logger.info(f"_echo: {update.message.text}")
    
    @staticmethod
    def _error(update, context):
        """Log Errors caused by Updates."""
        logger.warning('Update "%s" caused error "%s"', update, context.error)
   
    def add_handlers(self, updater) -> None:
        dp = updater.dispatcher

        # on different commands - answer in Telegram
        dp.add_handler(CommandHandler("start", AccountingBot._start))
        dp.add_handler(CommandHandler("help", AccountingBot._help))

        # on noncommand i.e message - echo the message on Telegram
        dp.add_handler(MessageHandler(Filters.text, AccountingBot._echo))

        # log all errors
        dp.add_error_handler(AccountingBot._error)

        # Start the Bot
        updater.start_polling()

    ### Utils ###
    @staticmethod
    def get_user_name(update: telegram.update.Update) -> str:
        user = update.message.from_user
        logger.info(f"get_user_name - user: {user}")
        return user

    def _get_all_commands() -> List[Callable]:
        logger.info("_get_all_commands")
        return ['start', 'help']


class AccountingBot(TelegramBot):
    allowed_users_ids: List[str] = []

    def __init__(self, telegram_token: str, allowed_users_ids: List[str]):
        super(AccountingBot, self).__init__(telegram_token)
        AccountingBot.allowed_users_ids = allowed_users_ids

    def check_id(endpoint) -> Callable:
        def wrapper(update, context):
            user = AccountingBot.get_user_name(update)
            if str(user['id']) in AccountingBot.allowed_users_ids:
                logger.info(f"check_id - allowed user: {user}")
                update.message.reply_text('Hi!')
                endpoint(update, context)
            else:
                logger.warning(f"Not authorized user! User: {user}")
                update.message.reply_text("Joey doesn't share food")

        return wrapper

    @check_id
    def _start(update, context) -> None:
        """Send a message when the command /start is issued."""
        update.message.reply_text('Hi!')
 
    @check_id
    def _help(update, context):
        update.message.reply_text(f'You can type the next commands: {AccountingBot._get_all_commands()}')
        logger.info("_help")
    
    @check_id
    def _echo(update, context):
        """Echo the user message."""
        update.message.reply_text(update.message.text)
        logger.info(f"_echo: {update.message.text}")
    
    @check_id
    def _error(update, context):
        """Log Errors caused by Updates."""
        logger.warning('Update "%s" caused error "%s"', update, context.error)
   

def main() -> None:
    '''Start Telegram Accounting bot'''

    bot_id = config('BOT_ID')
    allowed_users_ids = config('ALOWED_USERS').split(';')
    allowed_users_ids = [f.strip() for f in allowed_users_ids]

    ai_server_bot_telegram = AccountingBot(bot_id, allowed_users_ids)
    ai_server_bot_telegram.run()


if __name__ == '__main__':
    main()